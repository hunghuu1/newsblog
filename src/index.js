function ScoolToElement(idclick,idbuttom){
    if(typeof idclick === 'string' && typeof idbuttom === 'string'){

        /*check dữ liệu truyền vào hàm la chuoi thì mới chạy*/

        let click = document.querySelector(idclick);
        let buttom = document.querySelector(idbuttom);

        /*Kiem tra element co ton tai khong*/
        if(click && buttom){
            click.addEventListener('click',function(){
                buttom.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                    inline: "nearest",
                });
            })
        }
    }
}

ScoolToElement ('.beauty__text','#footer');

function scrollToMenu(idlist,idmenu){
    if( typeof idlist === 'string' && typeof idmenu === 'string'){

        let list = document.querySelector(idlist);
        let menu = document.querySelector(idmenu);
        let html = document.querySelector('html')
        if(list && menu){

            list.addEventListener('click',function(){
                menu.classList.toggle('active')
                html.classList.toggle('active')
            })
        }
    }
}

scrollToMenu('#item','#menu');
